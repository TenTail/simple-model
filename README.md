# Simple Model

## Introduction

一個簡單版本的Eloquent Model, 沒有資料庫相關設定的Model。所有 function, attributes 都盡量保留原有 [Laravel/framework](https://github.com/laravel/framework) 的設計。

有任何相關的 Feature 和 Bug 都歡迎提出。

## Installation

### Composer

`composer require tentail/simple-model`

## Functions

SimpleModel不連接資料庫，所以拔除以下功能：

- Illuminate\Database\Eloquent\Concerns\HasEvents
- Illuminate\Database\Eloquent\Concerns\HasGlobalScopes
- Illuminate\Database\Eloquent\Concerns\HasRelationships
- Illuminate\Database\Eloquent\Concerns\HasTimestamps
- protected $connection;
- protected $table;
- protected $primaryKey = 'id';
- protected $keyType = 'int';
- public $incrementing = true;
- ...等等，太多了這邊不一一列舉

保留的主要功能：

- Illuminate\Database\Eloquent\Concerns\GuardsAttributes
    - [x]  基本的 $fillable 和 $guarded 的功能。這個 trait 只作用於 SimpleModel 的 `fill()` 時。
- Illuminate\Database\Eloquent\Concerns\HasAttributes
    - [x]  protected $attributes = [];
    - [x]  protected $original = [];
    - [x]  protected $dateFormat;
    - [x]  protected $casts = [];
    - [x]  Accessors & Mutators
- Illuminate\Database\Eloquent\Concerns\HidesAttributes
    - [x]  protected $hidden = [];
    - [x]  protected $visible = [];
- ArrayAccess
- Arrayable
- Jsonable

## Usage

### 建立一個 SimpleModel

使用 `make:simple-model` Artisan 指令來快速建立 SimpleModel：

```
php artisan make:simple-model News
```

```php
<?php

namespace App;

use SimpleModel\SimpleModel;

class News extends SimpleModel
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
```

### 建立一個 SimpleModel 物件

```php
$news = new News($attributes);

$news = new News;
$news->fill($attributes);
```

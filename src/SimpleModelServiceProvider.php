<?php

namespace SimpleModel;

use Illuminate\Support\ServiceProvider;
use SimpleModel\Commands\SimpleModelMakeCommand;

class SimpleModelServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootCommands();
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {

    }

    public function bootCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                SimpleModelMakeCommand::class,
            ]);
        }
    }
}

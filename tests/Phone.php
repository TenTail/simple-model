<?php

namespace SimpleModel\Tests;

use SimpleModel\SimpleModel;

class Phone extends SimpleModel
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_launched' => 'boolean',
        'launched_date' => 'date',
        'price' => 'decimal:2',
        'support_music_type' => 'collection',
        'support_video_type' => 'collection',
    ];

    public function getDiscountPriceAttribute()
    {
        return $this->price * 0.9;
    }

    public function setWeightAttribute($value)
    {
        $this->attributes['weight'] = intval($value).'g';
    }
}

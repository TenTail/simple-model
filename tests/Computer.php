<?php

namespace SimpleModel\Tests;

use SimpleModel\SimpleModel;

class Computer extends SimpleModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'CPU',
        'memory',
        'GPU',
    ];
}

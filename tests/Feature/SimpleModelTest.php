<?php

namespace SimpleModel\Tests;

use SimpleModel\SimpleModel;

class SimpleModelTest extends TestCase
{
    /** @test */
    public function can_extends_SimpleModel()
    {
        $phone = new Phone;

        $this->assertTrue($phone instanceof SimpleModel);
    }
}

<?php

namespace SimpleModel\Tests;

class JsonableTest extends TestCase
{
    /** @test */
    public function can_toJson()
    {
        $attributes = [
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
            'is_launched' => 0,
            'launched_date' => '2019-01-20 00:00:00',
            'price' => 600,
            'support_music_type' => '["flac","mp3"]',
            'support_video_type' => '["mp4","mkv"]',
        ];

        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
            'is_launched' => 0,
            'launched_date' => '2019-01-20 00:00:00',
            'price' => 600,
            'support_music_type' => '["flac","mp3"]',
            'support_video_type' => '["mp4","mkv"]',
        ]);

        $json = json_encode([
            'name' => $phone->name,
            'size' => $phone->size,
            'weight' => $phone->weight,
            'is_launched' => $phone->is_launched,
            'launched_date' => '2019-01-20 00:00:00',
            'price' => $phone->price,
            'support_music_type' => $phone->support_music_type,
            'support_video_type' => $phone->support_video_type,
        ], 0);

        $this->assertEquals($phone->toJson(), $json);
    }
}

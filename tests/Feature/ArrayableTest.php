<?php

namespace SimpleModel\Tests;

class ArrayableTest extends TestCase
{
    /** @test */
    public function can_toArray()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
            'is_launched' => 0,
            'launched_date' => '2019-01-20 00:00:00',
            'price' => 600,
            'support_music_type' => '["flac","mp3"]',
            'support_video_type' => '["mp4","mkv"]',
        ]);

        $this->assertEquals($phone->toArray(), [
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
            'is_launched' => false,
            'launched_date' => '2019-01-20 00:00:00',
            'price' => '600.00',
            'support_music_type' => collect(['flac', 'mp3']),
            'support_video_type' => collect(['mp4', 'mkv']),
        ]);
    }
}

<?php

namespace SimpleModel\Tests;

use SimpleModel\Tests\Computer;

class GuardsAttributesTest extends TestCase
{
    /** @test */
    public function can_getFillable()
    {
        $computer = new Computer;

        $this->assertEquals($computer->getFillable(), ['name', 'CPU', 'memory', 'GPU']);
    }

    /** @test */
    public function can_setFillable()
    {
        $computer = new Computer;

        $computer->setFillable([
            'name',
            'CPU',
        ]);

        $this->assertEquals($computer->getFillable(), ['name', 'CPU']);
    }

    /** @test */
    public function can_getGuarded()
    {
        $computer = new Computer;

        $this->assertEquals($computer->getGuarded(), ['*']);
    }

    /** @test */
    public function can_setGuarded()
    {
        $computer = new Computer;

        $computer->setGuarded([
            'price',
        ]);

        $this->assertEquals($computer->getGuarded(), ['price']);
    }

    /** @test */
    public function test_fillable()
    {
        $computer = new Computer([
            'name' => 'Acer',
            'CPU' => 'i5',
            'memory' => '16G',
            'GPU' => 'GTX 1070Ti',
            'price' => 26000,
        ]);

        $this->assertEquals($computer->name, 'Acer');
        $this->assertEquals($computer->CPU, 'i5');
        $this->assertEquals($computer->memory, '16G');
        $this->assertEquals($computer->GPU, 'GTX 1070Ti');
        $this->assertNull($computer->price);
    }

    /** @test */
    public function test_guarded()
    {
        $computer = new Computer;

        $computer->setFillable([]);
        $computer->setGuarded(['price']);

        $computer->fill([
            'name' => 'Acer',
            'CPU' => 'i5',
            'memory' => '16G',
            'GPU' => 'GTX 1070Ti',
            'price' => 26000,
        ]);

        $this->assertEquals($computer->name, 'Acer');
        $this->assertEquals($computer->CPU, 'i5');
        $this->assertEquals($computer->memory, '16G');
        $this->assertEquals($computer->GPU, 'GTX 1070Ti');
        $this->assertNull($computer->price);
    }
}

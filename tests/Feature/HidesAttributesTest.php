<?php

namespace SimpleModel\Tests;

use SimpleModel\Tests\User;

class HidesAttributesTest extends TestCase
{
    /** @test */
    public function can_get_hidden()
    {
        $user = new User;

        $this->assertEquals($user->getHidden(), ['password']);
    }

    /** @test */
    public function can_set_hidden()
    {
        $user = new User;

        $user->setHidden(['email', 'password']);

        $this->assertEquals($user->getHidden(), ['email', 'password']);
    }

    /** @test */
    public function can_add_hidden()
    {
        $user = new User;

        $user->addHidden('email');

        $this->assertEquals($user->getHidden(), ['password', 'email']);
    }

    /** @test */
    public function can_get_visible()
    {
        $user = new User;

        $this->assertEquals($user->getVisible(), []);
    }

    /** @test */
    public function can_set_visible()
    {
        $user = new User;

        $user->setVisible(['email', 'password']);

        $this->assertEquals($user->getVisible(), ['email', 'password']);
    }

    /** @test */
    public function can_add_visible()
    {
        $user = new User;

        $user->addVisible('email');

        $this->assertEquals($user->getVisible(), ['email']);
    }

    /** @test */
    public function can_makeHidden()
    {
        $user = new User;

        $user->makeHidden('email');

        $this->assertEquals($user->getHidden(), ['password', 'email']);
    }

    /** @test */
    public function can_makeVisible()
    {
        $user = new User;

        $user->makeVisible(['name', 'email']);

        $this->assertEquals($user->getHidden(), ['password']);
        $this->assertEquals($user->getVisible(), []);
    }
}

<?php

namespace SimpleModel\Tests;

class HasAttributesTest extends TestCase
{
    //--------------------------------------------------------------------------
    // protected $attributes = [];
    //--------------------------------------------------------------------------

    /** @test */
    public function can_use_getAttribute_to_get_attributes()
    {
        $phone = new Phone;

        $this->assertNull($phone->getAttribute('name'));
    }

    /** @test */
    public function can_set_attributes_when_create_model()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertEquals($phone->getAttribute('name'), 'HTC U12 life');
        $this->assertEquals($phone->getAttribute('size'), '158.5 x 75.4 x 8.3 mm');
        $this->assertEquals($phone->getAttribute('weight'), '175g');
    }

    /** @test */
    public function can_dynamically_set_attributes()
    {
        $phone = new Phone;

        $phone->name = 'HTC U12 life';
        $phone->size = '158.5 x 75.4 x 8.3 mm';
        $phone->weight = '175g';

        $this->assertEquals($phone->getAttribute('name'), 'HTC U12 life');
        $this->assertEquals($phone->getAttribute('size'), '158.5 x 75.4 x 8.3 mm');
        $this->assertEquals($phone->getAttribute('weight'), '175g');
    }

    /** @test */
    public function can_dynamically_get_attributes()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertEquals($phone->name, 'HTC U12 life');
        $this->assertEquals($phone->size, '158.5 x 75.4 x 8.3 mm');
        $this->assertEquals($phone->weight, '175g');
    }

    //--------------------------------------------------------------------------
    // protected $original = [];
    //--------------------------------------------------------------------------

    /** @test */
    public function can_use_getOriginal_to_get_original_attributes()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertEquals($phone->getOriginal('name'), 'HTC U12 life');
    }

    /** @test */
    public function getOriginal_can_return_default_value()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertNull($phone->getOriginal('android'));
        $this->assertEquals($phone->getOriginal('android', 'Android™ 8.1 搭載 HTC Sense™'), 'Android™ 8.1 搭載 HTC Sense™');
    }

    /** @test */
    public function can_syncOriginal()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertNull($phone->getOriginal('android'));

        $phone->android = 'Android™ 8.1 搭載 HTC Sense™';
        $phone->syncOriginal();

        $this->assertEquals($phone->getOriginal('android'), 'Android™ 8.1 搭載 HTC Sense™');
    }

    /** @test */
    public function can_syncOriginalAttribute()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $phone->android = 'Android™ 8.1 搭載 HTC Sense™';
        $phone->camera = '1600 萬畫素 + 500 萬像素雙鏡頭';
        $phone->battery = '3600 mAh';

        $phone->syncOriginalAttribute('android');
        $this->assertEquals($phone->getOriginal('android'), 'Android™ 8.1 搭載 HTC Sense™');

        $phone->syncOriginalAttribute(['camera', 'battery']);
        $this->assertEquals($phone->getOriginal('camera'), '1600 萬畫素 + 500 萬像素雙鏡頭');
        $this->assertEquals($phone->getOriginal('battery'), '3600 mAh');
    }

    //--------------------------------------------------------------------------
    // protected $dateFormat = 'Y-m-d h:i:s';
    //--------------------------------------------------------------------------

    /** @test */
    public function can_get_dateFormat()
    {
        $phone = new Phone;

        $this->assertEquals($phone->getDateFormat(), 'Y-m-d H:i:s');
    }

    /** @test */
    public function can_set_dateFormat()
    {
        $phone = new Phone;

        $phone->setDateFormat('Y-m-d');

        $this->assertEquals($phone->getDateFormat(), 'Y-m-d');
    }

    //--------------------------------------------------------------------------
    // protected $casts = [];
    //--------------------------------------------------------------------------

    /** @test */
    public function can_get_casts()
    {
        $phone = new Phone;

        $this->assertEquals($phone->getCasts(), [
            'is_launched' => 'boolean',
            'launched_date' => 'date',
            'price' => 'decimal:2',
            'support_music_type' => 'collection',
            'support_video_type' => 'collection',
        ]);
    }

    /** @test */
    public function check_casts_type()
    {
        $phone = new Phone;

        $this->assertFalse($phone->hasCast('name'));
        $this->assertTrue($phone->hasCast('support_music_type'));
        $this->assertFalse($phone->hasCast('support_music_type', ['string', 'int']));
        $this->assertTrue($phone->hasCast('support_video_type', ['array', 'collection']));
    }

    /** @test */
    public function can_cast_attribute()
    {
        $phone = new Phone([
            'is_launched' => 0,
            'launched_date' => '2019-01-20 00:00:00',
            'price' => 600,
            'support_music_type' => '["flac","mp3"]',
            'support_video_type' => '["mp4","mkv"]',
        ]);

        $this->assertFalse($phone->is_launched);
        $this->assertEquals($phone->launched_date, \Illuminate\Support\Carbon::parse('2019-01-20 00:00:00'));
        $this->assertEquals($phone->price, '600.00');
        $this->assertEquals($phone->support_music_type, collect(['flac', 'mp3']));
        $this->assertEquals($phone->support_video_type, collect(['mp4', 'mkv']));
    }

    /** @test */
    public function set_attributes_can_be_cast()
    {
        $phone = new Phone;

        $phone->is_launched = 1;
        $phone->launched_date = \Illuminate\Support\Carbon::parse('2019-01-20');
        $phone->price = 600;
        $phone->support_music_type = ['flac', 'mp3'];
        $phone->support_video_type = collect(['mp4', 'mkv']);

        $phone->syncOriginal();
        $this->assertEquals($phone->getOriginal('is_launched'), 1);
        $this->assertEquals($phone->getOriginal('launched_date'), '2019-01-20 00:00:00');
        $this->assertEquals($phone->getOriginal('price'), 600);
        $this->assertEquals($phone->getOriginal('support_music_type'), '["flac","mp3"]');
        $this->assertEquals($phone->getOriginal('support_video_type'), '["mp4","mkv"]');

        $this->assertTrue($phone->is_launched);
        $this->assertEquals($phone->launched_date, \Illuminate\Support\Carbon::parse('2019-01-20 00:00:00'));
        $this->assertEquals($phone->price, '600.00');
        $this->assertEquals($phone->support_music_type, collect(['flac', 'mp3']));
        $this->assertEquals($phone->support_video_type, collect(['mp4', 'mkv']));
    }

    //--------------------------------------------------------------------------
    // Accessors & Mutators
    //--------------------------------------------------------------------------

    /** @test */
    public function can_set_attributes_by_mutator()
    {
        $phone = new Phone;

        $phone->weight = 175;
        $this->assertEquals($phone->weight, '175g');

        $phone->weight = '175';
        $this->assertEquals($phone->weight, '175g');

        $phone->weight = '175g';
        $this->assertEquals($phone->weight, '175g');

        $phone->weight = 'abc';
        $this->assertEquals($phone->weight, '0g');
    }

    /** @test */
    public function can_get_attributes_by_accessor()
    {
        $phone = new Phone;

        $phone->price = 600;

        $this->assertEquals($phone->discount_price, 600*0.9);
    }

    /** @test */
    public function can_get_mutated_attributes()
    {
        $phone = new Phone;

        $this->assertEquals($phone->getMutatedAttributes(), ['discount_price']);
    }
}

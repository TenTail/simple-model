<?php

namespace SimpleModel\Tests;

class ArrayAccessTest extends TestCase
{
    /** @test */
    public function test_offsetGet()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertEquals($phone['name'], 'HTC U12 life');
        $this->assertEquals($phone['size'], '158.5 x 75.4 x 8.3 mm');
        $this->assertEquals($phone['weight'], '175g');
    }

    /** @test */
    public function test_offsetExists()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
            'size' => '158.5 x 75.4 x 8.3 mm',
            'weight' => '175g',
        ]);

        $this->assertTrue(isset($phone['name']));
        $this->assertFalse(isset($phone['memory']));
    }

    /** @test */
    public function test_offsetSet()
    {
        $phone = new Phone;

        $phone['name'] = 'HTC U12 life';

        $this->assertEquals($phone->getAttribute('name'), 'HTC U12 life');
    }

    /** @test */
    public function test_offsetUnset()
    {
        $phone = new Phone([
            'name' => 'HTC U12 life',
        ]);

        unset($phone['name']);

        $this->assertNull($phone->getAttribute('name'));
    }
}
